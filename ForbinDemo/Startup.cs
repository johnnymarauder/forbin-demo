﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ForbinDemo.Startup))]
namespace ForbinDemo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
